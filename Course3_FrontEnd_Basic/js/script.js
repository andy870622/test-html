var bookDataFromLocalStorage = [];

$(function () {
  loadBookData();
  var data = [
    { text: "資料庫", value: "database.jpg" },
    { text: "網際網路", value: "internet.jpg" },
    { text: "應用系統整合", value: "system.jpg" },
    { text: "家庭保健", value: "home.jpg" },
    { text: "語言", value: "language.jpg" },
  ];
  $("#book_category").kendoDropDownList({
    dataTextField: "text",
    dataValueField: "value",
    dataSource: data,
    index: 0,
    change: onChange,
  });
  $("#bought_datepicker").kendoDatePicker({value:new Date(), format: "yyyy-MM-dd" });
  $("#book_grid").kendoGrid({
    dataSource: {
      type: "signalr",
      data: bookDataFromLocalStorage,
      schema: {
        model: {
          fields: {
            BookId: { type: "int" },
            BookName: { type: "string" },
            BookCategory: { type: "string" },
            BookAuthor: { type: "string" },
            BookBoughtDate: { type: "string" },
          },
        },
      },
      pageSize: 20,
    },
    // 新版 keno 支援
    // toolbar:["search"],
    toolbar: kendo.template(
      "<div class='book-grid-toolbar'><input class='book-grid-search' placeholder='我想要找......' type='text'></input></div>"
    ),
    height: "25em",
    pageable: {
      input: true,
      numeric: false,
    },
    columns: [
      { field: "BookId", title: "書籍編號", width: "10%" },
      { field: "BookName", title: "書籍名稱", width: "50%" },
      { field: "BookCategory", title: "書籍種類", width: "10%" },
      { field: "BookAuthor", title: "作者", width: "15%" },
      { field: "BookBoughtDate", title: "購買日期", width: "15%" },
      {
        command: { text: "刪除", click: deleteBook },
        title: " ",
        width: "120px",
      },
    ],
  });

  $(".book-grid-search").on("input", function () {
    $("#book_grid")
      .data("kendoGrid")
      .dataSource.filter({
        logic: "or",
        filters: [
          { field: "BookId", operator: "eq", value: $(this).val() },
          { field: "BookName", operator: "contains", value: $(this).val() },
          { field: "BookCategory", operator: "contains", value: $(this).val() },
          { field: "BookAuthor", operator: "contains", value: $(this).val() },
          {
            field: "BookBoughtDate",
            operator: "contains",
            value: $(this).val(),
          },
        ],
      });
  });
});

const bookValidator = $("#addBookForm")
  .kendoValidator({
    validate: function (e) { },
    messages: {
      required: "請填寫完整資訊!",
    },
  })
  .data("kendoValidator");

function showKendoPopUp() {
  clearErrorDiv();
  clearInput();
  onChange();
  var popup = $("#popUp")
    .kendoWindow({
      modal: true,
      resizable: false,
      pinned: true,
      draggable: false,
      title: "新增書籍",
      actions: ["Pin", "Maximize", "Close"],
      animation: {
        open: {
          effects: "zoom:in",
        },
        close: {
          effects: "zoom:out",
        },
      },
    })
    .data("kendoWindow");
  popup.center();
  popup.open();
  $(document).on("click", ".k-overlay", function () {
    popup.close();
  });
}

function loadBookData() {
  bookDataFromLocalStorage = JSON.parse(localStorage.getItem("bookData"));
  if (bookDataFromLocalStorage == null) {
    bookDataFromLocalStorage = bookData;
    localStorage.setItem("bookData", JSON.stringify(bookDataFromLocalStorage));
  }
}

function addBook() {
  if (!bookValidator.validate()) {
    bookValidator.hideMessages();
    $("#errorDiv").html("請確認資料是否完整!");
    return;
  }
  let data = bookDataFromLocalStorage;
  let id = data[data.length - 1].BookId + 1;
  data[data.length] = {
    BookId: id,
    BookCategory: $("#book_category option:selected").text(),
    BookName: $("#book_name").val(),
    BookAuthor: $("#book_author").val(),
    BookBoughtDate: $("#bought_datepicker").val(),
  };
  localStorage.setItem("bookData", JSON.stringify(data));
  $("#book_grid")
    .data("kendoGrid")
    .dataSource.add(data[data.length - 1]);
  showSuccessModal();
}

function deleteBook(e) {
  e.preventDefault();
  let data = this.dataItem($(e.currentTarget).closest("tr"));
  let id = $("#book_grid").data("kendoGrid").dataSource.indexOf(data);
  $("#book_grid").data("kendoGrid").dataSource.remove(data);
  bookDataFromLocalStorage.splice(id, 1);
  localStorage.setItem("bookData", JSON.stringify(bookDataFromLocalStorage));
}

function onChange() {
  $("#book-image")[0].src = "image/" + $("#book_category").val();
}

function clearErrorDiv() {
  $("#errorDiv").html("");
}

function clearInput() {
  $("#book_category").data("kendoDropDownList").text("資料庫");
  $("#book_name").val("");
  $("#book_author").val("");
  $("#bought_datepicker").val(new Date().toISOString().split("T")[0]);
}

function showSuccessModal() {
  let modal = $("#modal")
    .kendoWindow({
      modal: true,
      resizable: false,
      width: 200,
      title: "新增成功",
      animation: {
        open: {
          effects: "zoom:in",
        },
        close: {
          effects: "zoom:out",
        },
      },
    })
    .data("kendoWindow");
  $("#modal").parent(".k-window-titlebar").css({
    "background-color": "green",
  });
  modal.content("新增成功");
  modal.center();
  modal.open();
  $(document).on("click", ".k-overlay", function () {
    modal.close();
  });
}

// 輸入時清除錯誤訊息
$("input").bind("input", () => {
  clearErrorDiv();
});

